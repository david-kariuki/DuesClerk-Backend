<?php

/**
* Index file
* This file redirects users to the website
*
* @author David Kariuki (dk)
* @copyright Copyright (c) 2021 David Kariuki (dk) All Rights Reserved.
*/


// Header to redirect to website
header("Location: https://www.duesclerk.com");

// EOF : index.php
